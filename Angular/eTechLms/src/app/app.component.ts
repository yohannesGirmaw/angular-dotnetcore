import { Component } from '@angular/core';
import { Router } from '@angular/router';

export enum CourseTabs {
  edit,
  list,
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'eTechLms';
  opened!: boolean;
  events: string[] = [];
  courseTabs: typeof CourseTabs = CourseTabs;
  activeTab: CourseTabs = CourseTabs.edit;

}
