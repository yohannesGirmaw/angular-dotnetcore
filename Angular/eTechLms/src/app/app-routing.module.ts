import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SidenaveComponent } from './common-apps/sidenave/sidenave.component';
import { CourseDetailComponent } from './courses/course-detail/course-detail.component';
import { CourseEditComponent } from './courses/course-edit/course-edit.component';
import { CourseListComponent } from './courses/course-list/course-list.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: 'course-list', component:CourseListComponent},
  {path: 'course-edit', component:CourseEditComponent},

  {
    path: 'course', 
          component:  CourseDetailComponent,
          children: [
            {path: 'coursepage', component: CourseDetailComponent}
          ]
          }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const RoutingComponent = [ CourseDetailComponent, CourseListComponent,LoginComponent]