import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CourseModel } from '../courses/course.model';

@Injectable({
  providedIn: 'root'
})
export class CourseApiService {

  readonly courseAPIUrl = "https://localhost:7026/api";

  constructor(private http:HttpClient) { }

  getCourseList():Observable<any[]> {
    return this.http.get<any>(this.courseAPIUrl+'/CourseModels')
  }
  getCourseById(courseId: string): Observable<CourseModel> {
    return this.http.get<CourseModel>(this.courseAPIUrl + '/CourseModels/' + courseId);
  }
  addCourseList(data:any){
    return this.http.post(this.courseAPIUrl+'/CourseModels', data)
  }
  updateCourseList(id:number|string, data:any){
    return this.http.put(this.courseAPIUrl+`/CourseModels/${id}`, data)
  }
  deleteCourseList(id:number|string){
    return this.http.delete(this.courseAPIUrl+`/CourseModels/${id}`)
  }
}
