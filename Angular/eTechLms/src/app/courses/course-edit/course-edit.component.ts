import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition  } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { CourseApiService } from 'src/app/persist/course-api.service';
import { CourseModel } from '../course.model';

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.css']
})
export class CourseEditComponent implements OnInit {
  dataSaved = false;
  courseForm: any;
  employeeIdUpdate = null;
  massage = null;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  constructor(private formbulider: FormBuilder, private courseService: CourseApiService, public dialog: MatDialog,private _snackBar: MatSnackBar) { 

  }

  ngOnInit(): void {
    this.courseForm = this.formbulider.group({
      CourseName: ['', [Validators.required]],
      Instructor: ['', [Validators.required]],
      Pages: ['', [Validators.required]],
    });
  }

  onFormSubmit() {
    this.dataSaved = false;
    const employee = this.courseForm.value;
    this.CreateEmployee(employee);
    this.courseForm.reset();
  }

  CreateEmployee(course: CourseModel) {

      this.courseService.addCourseList(course).subscribe(
        () => {
          this.dataSaved = true;
          this.SavedSuccessful(1);
          this.employeeIdUpdate = null;
          this.courseForm.reset();
        }
      );
    }

SavedSuccessful(isUpdate: number) {
 if (isUpdate == 1) {
        this._snackBar.open('Record Saved Successfully!', 'Close', {
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
  }

  resetForm() {
    this.courseForm.reset();
    this.massage = null;
    this.dataSaved = false;
  }
}
  
