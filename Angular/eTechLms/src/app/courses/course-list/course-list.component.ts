import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import {CourseApiService} from 'src/app/persist/course-api.service'
import { CourseModel } from '../course.model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  dataSaved = false;
  courseForm: any;
  dataSource!: MatTableDataSource<CourseModel>;
  selection = new SelectionModel<CourseModel>(true, []);
  employeeIdUpdate = 0;
  massage = null;

  SelectedDate = null;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  displayedColumns: string[] = ['select', 'CourseName', 'Instructor', 'Pages', 'Edit', 'Delete'];
  // @ViewChild(MatPaginator)
  // paginator!: MatPaginator;
  // @ViewChild(MatSort)
  // sort!: MatSort;
  courseList!:Observable<any[]>;

  constructor(private service: CourseApiService, public dialog: MatDialog, private _snackBar: MatSnackBar,private formbulider: FormBuilder,) {
    this.service.getCourseList().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      // this.dataSource.paginator = this.paginator;
      // this.dataSource.sort = this.sort;
    });
  }

  ngOnInit(): void {

    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    this.courseList = this.service.getCourseList();
    this.courseForm = this.formbulider.group({
      CourseName: ['', [Validators.required]],
      Instructor: ['', [Validators.required]],
      Pages: ['', [Validators.required]],
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = !!this.dataSource && this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(r => this.selection.select(r));
  }

  checkboxLabel(row: CourseModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  loadAllCourse() {
    this.service.getCourseList().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      // this.dataSource.paginator = this.paginator;
      // this.dataSource.sort = this.sort;
    });
  }
  loadCourseToEdit(employeeId: string) {
    this.service.getCourseById(employeeId).subscribe(employee => {
      this.massage = null;
      this.dataSaved = false;
      console.log(employee);
      this.employeeIdUpdate = employee.id;

      this.courseForm.controls.CourseName.setValue('employee.courseName')
      this.courseForm.controls.Instructor.setValue('employee.instructor')
      this.courseForm.controls.Pages.setValue('employee.pages')

    });
  }
  
  CreateEmployee(employee: CourseModel) {
      employee.id = this.employeeIdUpdate;
      this.service.updateCourseList(employee.id, employee).subscribe(() => {
        this.dataSaved = true;
        this.SavedSuccessful(0);
        this.loadAllCourse();
        this.employeeIdUpdate = 0;
        // this.employeeForm.reset();
      });
    
  }
  deleteEmployee(employeeId: string) {
    if (confirm("Are you sure you want to delete this ?")) {
      this.service.deleteCourseList(employeeId).subscribe(() => {
        this.dataSaved = true;
        this.SavedSuccessful(2);
        this.loadAllCourse();
        this.employeeIdUpdate = 0;
        // this.employeeForm.reset();

      });
    }

  }
  SavedSuccessful(isUpdate:any) {
    if (isUpdate == 0) {
      this._snackBar.open('Record Updated Successfully!', 'Close', {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
    else if (isUpdate == 1) {
      this._snackBar.open('Record Saved Successfully!', 'Close', {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
    else if (isUpdate == 2) {
      this._snackBar.open('Record Deleted Successfully!', 'Close', {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }
}
