export class CourseModel {

    constructor(
      public id: number,
      public courseName: string,
      public instructor: string,
      public pages: number,
    ) {  }
  
  }