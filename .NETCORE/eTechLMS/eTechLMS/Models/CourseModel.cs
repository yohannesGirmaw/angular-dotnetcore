﻿using System.ComponentModel.DataAnnotations;

namespace eTechLMS.Models
{
    public class CourseModel
    {
        [Key]
        public int Id { get; set; }
        public string? CourseName { get; set; }
        public string? Instructor { get; set; }
        public int? Pages { get; set; }

    }
}
