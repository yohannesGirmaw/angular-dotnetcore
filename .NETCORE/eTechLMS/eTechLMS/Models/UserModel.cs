﻿
using System.ComponentModel.DataAnnotations;
namespace eTechLMS.Models
{
    public class UserModel
    {
        [Key]
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? UserName { get; set; }
        public int? Age { get; set; }
    }
}

