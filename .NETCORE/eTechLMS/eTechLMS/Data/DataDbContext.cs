﻿
using Microsoft.EntityFrameworkCore;

namespace eTechLMS.Data
{
    public class DataDbContext : DbContext
    {
        public DataDbContext(DbContextOptions<DataDbContext> options) : base(options) { }

        public DbSet<eTechLMS.Models.UserModel> UserModel { get; set; }

        public DbSet<eTechLMS.Models.CourseModel> CourseModel { get; set; }

    }
}
